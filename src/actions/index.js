import getData from "./generic/getData";

const actions = {
  getData
};

export default actions;
