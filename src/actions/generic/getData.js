import { GET_DATA } from "./../const";

function action (data) {
  return dispatch => {
    dispatch({ type: GET_DATA, payload: data });
  };
}

export default action;
