import { GET_DATA } from "./../actions/const";

function reducer(state = null, action) {
  const nextState = Object.assign({}, state);
  switch (action.type) {
    case GET_DATA:
      nextState.storeData = action.payload;
      return nextState;
    default:
  }
  return state;
}

export default reducer;
