import { combineReducers } from "redux";
import generic from "./generic";

const rootReducer = combineReducers({
  generic: generic
});

export default rootReducer;
