import React, { Component } from "react";
import './App.css';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import actions from "./actions";

class App extends Component {
  constructor () {
    super();
    this.state = {
      stepCount: 0,
      noList: [],
      success: false
    }
    this.startNewGame = this.startNewGame.bind(this);
    this.handleTileClick = this.handleTileClick.bind(this);
  }

  componentDidMount() {
    let arr = [];
    for(let i=0; i<15; i++){
      arr.push(i + 1);
    }
    arr.push(0);
    this.setState({ noList: arr });
  }

  startNewGame() {
    let newArr = this.state.noList;
    for (let i = newArr.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [newArr[i], newArr[j]] = [newArr[j], newArr[i]];
    }
    this.setState({ noList: newArr });
    this.setState({ stepCount: 0 });
    this.setState({ success: false });
  }

  handleChange = () =>{}

  handleTileClick(val) {
    if (!this.state.success && val){
      let noList = this.state.noList;
      let index = noList.indexOf(val);
      if (noList[index + 4] === 0) {
        this.handleTileClickChanges(noList, index + 4, index , val );
      } else if (noList[index - 4] === 0) {
        this.handleTileClickChanges(noList, index - 4, index, val);
      } else if ((index + 1) % 4 !== 0 && noList[index + 1] === 0) {
        this.handleTileClickChanges(noList, index + 1, index, val);
      } else if ((index + 1) % 4 !== 1 && noList[index - 1] === 0) {
        this.handleTileClickChanges(noList, index - 1, index, val);
      }
    }
  }

  handleTileClickChanges (noList, newIndex, oldIndex, val) {
    noList[newIndex] = val;
    noList[oldIndex] = 0;
    this.setState({ stepCount: this.state.stepCount + 1 });
    this.setState({ noList: noList });

    const { actions } = this.props;
    actions && actions.getData(noList);

    let originalList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0];
    for (let i = 0; i < noList.length; i++) {
      if (noList[i] !== originalList[i]) {
        break;
      }
      if (this.state.stepCount > 0 && i === noList.length - 1) {
        this.setState({ success: true });
      }
    }
  }

  render () {
    const { noList, stepCount, success } = this.state;
    return (
      <>
        { noList && noList.map((val, i) => (
          <div key={ i } className="block"
           style={ { background: val === 0 ? 'white' : 'black' } }
            onClick={ () => this.handleTileClick(val) }>
            { val }
          </div>
        ))}

        <div className="start-game" onClick={ this.startNewGame }>Start New Game</div>
        <div className="step-counter">Step Counter <input id="step-counter" type="text" value={ stepCount } onChange={ this.handleChange } /></div>
        { success && <div className="success">Congratulations you have successfully completed the game.</div>}
      </>
    );
  }
}

function mapStateToProps (state) {
  return { generic: state.generic };
}

function mapDispatchToProps (dispatch) {
  const actionMap = { actions: bindActionCreators(actions, dispatch) };
  return actionMap;
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
